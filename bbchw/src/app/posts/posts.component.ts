import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { Observable } from 'rxjs';
import { postsService } from '../posts.service';
import { ActivatedRoute } from '@angular/router';
import{Users} from '../interfaces/users';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;

  /*
  posts$: Posts[];
  users$: Users[];
  title:string;
  body:string;
  author:string;
  ans:string;
*/
posts$:Observable<any[]>;


  constructor(private route: ActivatedRoute, private postService:postsService ) { }
/*
  myFunc(){
    for (let index = 0; index < this.posts$.length; index++) {
      for (let i = 0; i < this.users$.length; i++) {
        if (this.posts$[index].userId==this.users$[i].id) {
          this.title = this.posts$[index].title;
          this.body = this.posts$[index].body;
          this.author = this.users$[i].name;
          this.postService.addPosts(this.body,this.author,this.title);   
        }  
      } 
    }
  this.ans ="The data retention was successful"
  }

  */
  ngOnInit() {
    this.posts$ = this.postService.getPosts()
    }

    deletePost(id:string){
      this.postService.deletePost(id);
  
    }


    /*this.postData$ = this.postService.searchPostData();
    this.users = this.postService.getUsers();
    

    this.postData$.subscribe(
      data => {
        this.posts = data;
        console.log(this.posts);
      }

    ) 
  }
  */
  }

