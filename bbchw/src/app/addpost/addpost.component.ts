import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { postsService } from '../posts.service';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {


  constructor(private postsservice:postsService, private router:Router, private route:ActivatedRoute) { }

  title:string;
  body:string;
  author:string;
  id:string; 
  isEdit:boolean = false;
  buttonText:string = 'Add post';
  
  onSubmit(){ 
    if(this.isEdit){
      console.log('edit mode');
      this.postsservice.updatePost(this.id,this.title,this.author,this.body);
    } else {
      this.postsservice.addPost(this.title,this.author,this.body);
    }
    this.router.navigate(['/posts']);  
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    console.log(this.id);
    if(this.id) {
      this.isEdit = true;
      this.buttonText = 'Update Post'   
      this.postsservice.getPost(this.id).subscribe(
        post => {
          console.log(post.data().author)
          console.log(post.data().title)
          this.title = post.data().title; 
          this.author = post.data().author;
          this.body = post.data().body; 
          
        }
      )
     }
  }
 

}
