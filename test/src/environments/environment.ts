// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig : {
    apiKey: "AIzaSyDH3FrFsrqtma2EJnSyvKTaU3lgJBnTGIk",
    authDomain: "testanexp.firebaseapp.com",
    databaseURL: "https://testanexp.firebaseio.com",
    projectId: "testanexp",
    storageBucket: "testanexp.appspot.com",
    messagingSenderId: "817732793287",
    appId: "1:817732793287:web:eb9e443c3907c8c1979fe2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
