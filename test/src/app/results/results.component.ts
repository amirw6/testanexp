import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  results$:Observable<any>;

  constructor(public classifyservice:ClassifyService) { }

  ngOnInit() {

    this.results$= this.classifyservice.getDocs();
    console.log(this.results$)
  } 
  
}
