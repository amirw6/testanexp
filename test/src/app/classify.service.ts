import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  docCollection: AngularFirestoreCollection = this.db.collection('users');

  private url = "https://f3602zqh5k.execute-api.us-east-1.amazonaws.com/beta150";

  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};
  public doc:string;

  classify():Observable<any>{
    let json = {
      "articles": [
        {"text": this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    )
  }


  addDoc(category:string,body:string){
    
      const doc = {category:category, body:body}
      this.db.collection('docs').add(doc);
      
      this.router.navigate(['/results']);
    }
    
  
  getDocs():Observable<any[]>{
    return this.db.collection('docs').valueChanges();
  }
  



  constructor(public http:HttpClient, private db:AngularFirestore, public router:Router ){ }
}
