import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  body:string;
  categorychange:string;

  categories:object[] = [{name:'business'}, {name:'entertainment'}, {name: 'politics'}, {name: 'sport'},{name:'tech'}];

  
  constructor(public classifyservice:ClassifyService, public router:Router ) { }


  ngOnInit() {
    this.classifyservice.classify().subscribe(
      res => {
        this.category = this.classifyservice.categories[res];
        this.body = this.classifyservice.doc
      }
    )
  }

  passDifferentcategory(){
    
    this.classifyservice.addDoc(this.categorychange,this.body);

  }

  passDeafultcategory(){
    this.classifyservice.addDoc(this.category,this.body);
  }


}
