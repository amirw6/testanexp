import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Posts } from './interfaces/posts';
import { Observable, forkJoin } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private PostURL = "https://jsonplaceholder.typicode.com/posts/";
  private UserURL = "https://jsonplaceholder.typicode.com/users/";


  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postsCollection: AngularFirestoreCollection;
  
  constructor(private http:HttpClient, private db:AngularFirestore) { }

  getPosts(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'});
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
    return this.postsCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            return data;
          }
        )
  
      )
    )
  }
  
  getPost(id:string, userId:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()
  }
  
  
  addPosts(userId:string,title:string,author:string){
     const post = {title:title, author:author}
      //this.db.collection('posts').add(post);
      this.userCollection.doc(userId).collection('posts').add(post);
  }
  deletePost(userId:string,id:string){
     this.db.doc(`users/${userId}/posts/${id}`).get()
 }

 updatePost(userId:string, id:string, title:string, author:string){
  this.db.doc(`users/${userId}/posts/${id}`).update({
    title:title,
    author:author
  })
}


    /*
      let getPosts = this.http.get(this.PostURL);
      let getUsers = this.http.get(this.UserURL);
      
      // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
      return forkJoin([getPosts, getUsers]);
    }
    */

    /*
    addPosts(title:string, body:string ,author:string){
      const post = {title:title, body:body ,author:author}
      this.db.collection('posts').add(post);
    }
    */
    
  }



