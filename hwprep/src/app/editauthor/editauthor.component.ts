import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {

  authorname:string;
  authorid:string;
  constructor(private router:Router,public activatedroute:ActivatedRoute) { }

  ngOnInit() {
  }
  onSubmit(){
    this.authorid = this.activatedroute.snapshot.params['id'];
    this.router.navigate(['/authors', this.authorid, this.authorname]);
    
  }

  }


