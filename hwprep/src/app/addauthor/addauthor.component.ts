import { Component, OnInit } from '@angular/core';
import { AuthorsService } from '../authors.service';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addauthor',
  templateUrl: './addauthor.component.html',
  styleUrls: ['./addauthor.component.css']
})
export class AddauthorComponent implements OnInit {

  authorname:string;
  

  constructor(private authorsservice:AuthorsService, public router:Router) { }
 
  onSubmit(){
    this.authorsservice.addAuthor(this.authorname)
    this.router.navigate(['authors/9/amir']);
  }
  ngOnInit() {
  }

}
