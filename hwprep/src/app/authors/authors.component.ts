import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorsService } from '../authors.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {

  authors:any;
  authors$:Observable<any>;
  authornamenew:string;
  id:string;
  
  constructor(private activatedroute:ActivatedRoute,private authorsservice:AuthorsService) { }

  ngOnInit() {
    this.authors$ = this.authorsservice.getAuthors();
  }

}
