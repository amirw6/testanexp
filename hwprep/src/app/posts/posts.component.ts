import { Component, OnInit } from '@angular/core';
import { HttpBackend } from '@angular/common/http';
import { PostsService } from '../posts.service';
import { Posts } from '../interfaces/posts';
import { Users } from '../interfaces/users';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<any>;
  id:string;
  userId:string
  /*
  public posts: any;
  public users: any;
  title:string;
  body:string;
  author:string;
  */
  

  constructor(public http:HttpBackend,public postsservice:PostsService,
    public route:Router,private activeroute:ActivatedRoute, public authservice:AuthService) { }

  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.postsservice.getPosts(this.userId);
      }
    )
   }
 
    
  deletePost(id:string){
    this.postsservice.deletePost(this.userId,id)
  }
  
  






    /*


  
      this.postsservice.getPostsandUsers().
      subscribe(data =>{
        this.posts = data[0];
        this.users = data[1];
         console.log(this.posts);
         console.log(this.users);
        
      });
    }

      PostPosts(){
          
        for(let i=0; i<this.posts.length;i++){
          for(let j=0; j<this.users.length;j++){
            if(this.posts[i].userId==this.users[j].id){
              console.log('Im inside of you')
              this.title = this.posts[i].title;
              this.body = this.posts[i].body;
              this.author = this.users[j].name;
              this.postsservice.addPosts(this.title, this.body, this.author)
            }
 
          }
          

        }
        


      }
      */





  
}

